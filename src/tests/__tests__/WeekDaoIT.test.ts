import {
  TestDBManager,
  convertInterfaceToObjectList
} from "@tests/TestDbManager";

import { getWeek, upsertMealToDay, removeMealFromWeek } from "@weeks/WeekDao";
import { Meal } from "@meals/Meal";

var testDB: TestDBManager;
beforeAll(async () => {
  testDB = new TestDBManager();
  await testDB.start();
  await testDB.cleanup();
  await testDB.insertTestData();
});

afterAll(async () => {
  await testDB.stop();
});

test("[getWeek] With Start Date -> Returns Next 7 Days (3 days) ", async () => {
  let listOfMealsInWeek = await convertInterfaceToObjectList(
    getWeek("2018-12-17")
  );
  let expectedOutcome = [
    {
      day: "2018-12-17",
      dinner: {
        name: "Dominos",
        tags: ["takeout"]
      }
    },
    {
      day: "2018-12-18",
      lunch: {
        name: "Pesto Pasta",
        tags: ["vegie"]
      },
      dinner : {
        name : "HealthyTakeout",
        tags : [ 
            "vegie", 
            "treat"
        ]
      }
    },
    {
      day: "2018-12-19",
      dinner: {
        name: "Pie & Chips",
        tags: ["comfort"]
      }
    }
  ];

  expect(listOfMealsInWeek.length).toBe(3);
  expect(listOfMealsInWeek).toMatchObject(expectedOutcome);
});

test("[getWeek] With Start Date -> Returns Next 7 Days (1 day)", async () => {
  let listOfMealsInWeek = await convertInterfaceToObjectList(
    getWeek("2019-01-01")
  );
  let expectedOutcome = [
    {
      day: "2019-01-01",
      dinner: {
        name: "Chinese",
        tags: ["takeout"]
      }
    }
  ];

  expect(listOfMealsInWeek.length).toBe(1);
  expect(listOfMealsInWeek).toMatchObject(expectedOutcome);
});

test("[getWeek] With Start Date -> Returns Next 2 Days (2 day)", async () => {
  let listOfMealsInWeek = await convertInterfaceToObjectList(
    getWeek("2018-12-17", 2)
  );

  let expectedOutcome = [
    {
      day: "2018-12-17",
      dinner: {
        name: "Dominos",
        tags: ["takeout"]
      }
    },
    {
      day: "2018-12-18",
      lunch: {
        name: "Pesto Pasta",
        tags: ["vegie"]
      },
      dinner : {
        name : "HealthyTakeout",
        tags : [ 
            "vegie", 
            "treat"
        ]
      }
    }
  ];

  expect(listOfMealsInWeek.length).toBe(2);
  expect(listOfMealsInWeek).toMatchObject(expectedOutcome);
});

test("[getWeek] With Start Date -> Returns Next 2 Days (0 days)", async () => {
  let listOfMealsInWeek = await convertInterfaceToObjectList(
    getWeek("2020-12-17", 2)
  );

  expect(listOfMealsInWeek.length).toBe(0);
});

test("[upsertMealToDay] Update meal for day -> meal update correctly", async () => {
  const DATE: string = '2018-12-18';
  const newMeal = new Meal('newDinnerMeal', ['tag1','tag2']);
  
  let dayOriginal: any = await convertInterfaceToObjectList(getWeek(DATE, 1));
  await upsertMealToDay(DATE, 'dinner', newMeal);
  let dayChanged: any = await convertInterfaceToObjectList(getWeek(DATE, 1));
  
  expect((<Meal>dayOriginal[0].dinner).name).toEqual('HealthyTakeout');
  expect((<Meal>dayChanged[0].dinner).name).toEqual('newDinnerMeal');
  expect(dayChanged).not.toMatchObject(dayOriginal);
});

test("[upsertMealToDay] Update meal for day -> No Meal Exists -> Inserts Meal", async () => {
  const DATE: string = '2018-12-19';
  const newMeal = new Meal('newLunchMeal', ['tag1','tag2']);
  
  let dayOriginal: any = await getWeek(DATE, 1);
  await upsertMealToDay(DATE, 'lunch', newMeal);
  let dayChanged: any = await getWeek(DATE, 1);

  expect((<Meal>dayOriginal[0].lunch)).toEqual(undefined);
  expect((<Meal>dayChanged[0].lunch).name).toEqual('newLunchMeal');
  expect(dayOriginal).not.toMatchObject(dayChanged);
});

test("[removeMeal] Remove Lunch From Week -> Lunch undefined ", async() => {
  const DATE = '2018-12-18';
  let day: any = await getWeek(DATE, 1);
  await removeMealFromWeek(DATE, 'lunch');
  let newDay: any = await getWeek(DATE, 1);

  expect((<Meal>day[0].lunch).name).toEqual('Pesto Pasta');
  expect(<Meal>newDay[0].lunch).toEqual(undefined);
});