import { TestDBManager, convertInterfaceToObjectList } from "@tests/TestDbManager";
import { getMeals, addMeal, removeMealsByIdList } from "@meals/MealDao";
import { Meal } from "@meals/Meal";

import mealsTestData from "@tests/testData/mealsTestData.json";
import { MealDocument } from "@meals/MealSchema";

var testDB: TestDBManager;

beforeAll(async () => {
  testDB = new TestDBManager();
  await testDB.start();
  await testDB.cleanup();
  await testDB.insertTestData();
});

afterAll(async () => {
  await testDB.stop();
});

test("[getMeals] Empty Params -> Returns All Meals", async () => {
  let listOfAllMeals = await convertInterfaceToObjectList(getMeals());

  expect(listOfAllMeals.length).toBe(3);
  expect(listOfAllMeals).toMatchObject(mealsTestData);
});

test("[getMeals] Partial Search Term 'dom' -> Returns 1 Matching Meals", async () => {
  let queryResult = await convertInterfaceToObjectList(getMeals("dom"));

  let expectedResult = [{
    name: "Dominos",
    tags: ["takeout"]
  }];

  expect(queryResult.length).toBe(1);
  expect(queryResult).toMatchObject(expectedResult);
});

test("[getMeals] Filter by tags ['vegan','treat'] -> Returns 1 Matching Meals", async () => {
    let queryResult = await convertInterfaceToObjectList(getMeals(undefined, ['vegie', 'treat']));

    let expectedResult = [
        {
            "name" : "HealthyTakeout",
            "tags" : [ 
                "vegie", 
                "treat"
            ]
        }
    ];

    expect(queryResult).toMatchObject(expectedResult);
});

test("[getMeals] Partial Search Term 'pasta' with tag 'vegan' -> Returns 1 Matching Meals", async () => {
    let queryResult = await convertInterfaceToObjectList(getMeals('pasta', ['vegie']));

    let expectedResult = [
        {
            "name" : "Pesto Pasta",
            "tags" : [ 
                "vegie"
            ]
        }
    ];

    expect(queryResult).toMatchObject(expectedResult);
});

test("[getMeals] Unknown Search Term -> Returns 0 Meals", async () => {
    let queryResult = await convertInterfaceToObjectList(getMeals('pasta2', ['vegie']));

    expect(queryResult.length).toBe(0);
});

test("[addMeal] Valid Meal -> Meal Added To Collection", async () => {
    let newMeal: Meal = new Meal("NewTastyMeal",["vegie", "takeout"]);
    let queryResult = await addMeal(newMeal);

    expect(queryResult).toHaveProperty('_id');

    let findResult = await convertInterfaceToObjectList(getMeals('NewTasty', ['vegie']));

    expect(findResult.length).toBe(1);
    expect(findResult).toMatchObject([newMeal]);
});

test("[addMeal] Invalid Meal -> Missing Name -> Throws Exception", async () => {
    let newMeal: any = {
        "name": "error"
    }

    expect(addMeal(newMeal)).rejects.toThrowError(Error);

    let findResult = await convertInterfaceToObjectList(getMeals('error'));

    expect(findResult.length).toBe(0);
});

test("[removeMeal] List of Ids -> Meals are removed", async () => {
    
    let newMeal1: MealDocument = await addMeal(new Meal('newMeal1', ['testTag']));
    let newMeal2: MealDocument = await addMeal(new Meal('newMeal2', ['testTag']));
    let findResult = await convertInterfaceToObjectList(getMeals('newMeal'));

    expect(findResult.length).toBe(2);

    removeMealsByIdList([newMeal1._id, newMeal2._id]);
    findResult = await convertInterfaceToObjectList(getMeals('newMeal'));

    expect(findResult.length).toBe(0);
});