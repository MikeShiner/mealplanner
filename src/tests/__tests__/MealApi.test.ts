import Server from '@app/Server';
import superRequest from 'supertest';
import Sinon from 'sinon';
import { MealModel, MealDocument } from '@app/meals/MealSchema';
import { Meal } from '@app/meals/Meal';

var server: Server;
var expressApi: Express.Application;

var sandbox: Sinon.SinonSandbox;
const TEST_MEAL: Array<Meal> = [new Meal('Dominos', ['takeout'])];

beforeAll(async () => {
    server = await new Server(1234);
    expressApi = server.getExpressApp();
});

afterAll(async () => {
    await server.stop();
});

beforeEach(() => {
    sandbox = Sinon.createSandbox();
});

afterEach(() => {
    sandbox.restore();
});

test('[GET] /meals -> returns expected meals with 200', async () => {
    // var spy = Sinon.spy(getMeals);
    sandbox
        .stub(MealModel, 'find')
        .withArgs({})
        .returns(<any>TEST_MEAL);

    return superRequest(expressApi)
        .get('/meals')
        .expect(200, JSON.stringify(TEST_MEAL));
});

test('[GET] /meals -> malformed tag filter with 400', async () => {
    return superRequest(expressApi)
        .get('/meals?tags=[one]')
        .expect(400, {});
});

test('[GET] /meals -> valid search/tag filter -> returns meal with 200', async () => {
    let tags = ['takeout'];
    let search = 'dom';

    sandbox
        .stub(MealModel, 'find')
        .withArgs({
            name: new RegExp(search, 'i'),
            tags: { $all: tags }
        })
        .returns(<any>TEST_MEAL);

    return superRequest(expressApi)
        .get(`/meals?tags=["${tags}"]&search=${search}`)
        .expect(200, JSON.stringify(TEST_MEAL));
});

// test('[PUT] /meals -> valid meal -> returns meal with 200', async () => {
//     let meal: Meal = new Meal("validTestName", ["valid", "tags"]);
//     let mealModel: MealDocument = new MealModel(meal);
//   sandbox
//       .stub(mealModel, 'save')
//       .returns(<any>TEST_MEAL);
//     console.log(meal)
//   return superRequest(expressApi)
//       .put(`/meals`)
//       .type('json')
//       .set('Accept', 'application/json')
//       .send(meal)
//       .expect(200, JSON.stringify(meal));
// });
