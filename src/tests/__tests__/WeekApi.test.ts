import Server from '@app/Server';
import superRequest from 'supertest';
import Sinon from 'sinon';
import { WeekModel } from '@app/weeks/WeekSchema';
import { Meal } from '@app/meals/Meal';

var server: Server;
var expressApi: Express.Application;

var sandbox: Sinon.SinonSandbox;
const TEST_WEEK: Array<object> = [
    {
        "day" : "2018-01-01",
        "dinner" : {
            "name" : "Dominos",
            "tags" : [ 
                "takeout"
            ]
        }
    }];

beforeAll(async () => {
    server = await new Server(1234);
    expressApi = server.getExpressApp();
});

afterAll(async () => {
    server.stop();
    await server.stop();
});

beforeEach(() => {
    sandbox = Sinon.createSandbox();
});

afterEach(() => {
    sandbox.restore();
});

test('[GET] /week/2018-01-01?range=1 -> returns expected meals with 200', async () => {
    sandbox
        .stub(WeekModel, 'find')
        .withArgs({ day: { $in: ['2018-01-01'] } })
        .returns(<any>{
            select: function() {
                return TEST_WEEK;
            }
        });

    return superRequest(expressApi)
        .get('/week/2018-01-01?range=1')
        .expect(200, JSON.stringify(TEST_WEEK));
});

test('[PUT] /week/2018-01-01/lunch -> Valid meal/type -> returns 200', async () => {
    let newMeal: Meal = new Meal("testMeal", ["valid"]);
    let type = 'lunch';
    let date = '2018-01-01';
    let expectedOutcome = [{
        "date": "2018-01-01",
        "lunch": newMeal
    }];
    
    sandbox
        .stub(WeekModel, 'findOneAndUpdate')
        .withArgs({day: date}, {[type]: newMeal}, {upsert: true})
        .returns(<any>{
            exec: function() {
                return expectedOutcome;
            }
        });

    return superRequest(expressApi)
        .put(`/week/${date}/${type}`)
        .type('json')
        .send(JSON.stringify(newMeal))
        .set('Accept', 'application/json')
        .expect(200, JSON.stringify(expectedOutcome));
});

test('[PUT] /week/2018-01-01/lunch -> invalid meal -> returns 400', async () => {
    let type = 'lunch';
    let date = '2018-01-01';

    return superRequest(expressApi)
        .put(`/week/${date}/${type}`)
        .type('json')
        .send({
            name: 'malformed'
        })
        .set('Accept', 'application/json')
        .expect(400);
});

test('[PUT] /week/2018-01-01/lunch -> invalid type -> returns 400', async () => {
    let type = 'lunch2';
    let date = '2018-01-01';

    return superRequest(expressApi)
        .put(`/week/${date}/${type}`)
        .type('json')
        .set('Accept', 'application/json')
        .send({
            name: 'valid',
            tags: ['1']
        })
        .expect(400);
});

test('[DELETE] /week/2018-01-01/lunch -> invalid type -> returns 400', async () => {
    let newMeal: Meal = new Meal("testMeal", ["valid"]);
    let type = 'lunch';
    let date = '2018-01-01';
    let expectedOutcome = [{
        "date": "2018-01-01",
        "lunch": newMeal
    }];
    
    sandbox
        .stub(WeekModel, 'findOneAndUpdate')
        .withArgs({day: date}, {$unset:{[type]: 1}}, {})
        .returns(<any>expectedOutcome);

    return superRequest(expressApi)
        .delete(`/week/${date}/${type}`)
        .expect(200, JSON.stringify(expectedOutcome));
});