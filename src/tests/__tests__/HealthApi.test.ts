import Server from "@app/Server";
import superRequest from "supertest";
import Mongoose from "mongoose";

var server: Server;
var expressApi: Express.Application;

beforeAll(async () => {
  server = await new Server(1234);
  expressApi = server.getExpressApp();
});

afterAll(async () => {
    await server.stop();
});

test("[GET] /health -> returns 200", async () => {
  return superRequest(expressApi)
    .get("/health")
    .expect(200);
});

test("[GET] /health/db -> Mongo started -> returns 200", async () => {

    return superRequest(expressApi)
      .get("/health")
      .expect(200);

  });
