import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { MealModel } from '@meals/MealSchema';
import { WeekModel } from '@weeks/WeekSchema';

import mealsTestData from '@testData/mealsTestData.json';
import weekTestData from '@testData/weekTestData.json';

// Extend the default timeout so MongoDB binaries can download
jest.setTimeout(60000);

export class TestDBManager {
    private server: MongoMemoryServer | undefined;

    constructor() {}

    async start() {
        (<any>mongoose).Promise = global.Promise;
        if (process.env.inmemorydb) {
            this.server = new MongoMemoryServer();
            const url = await this.server.getConnectionString();
            await mongoose.connect(url, { useNewUrlParser: true });
        } else {
            await mongoose.connect('mongodb://mongo/mealplanner-test', {
                useNewUrlParser: true
            });
        }

    }    
    
    stop() {
        this.cleanup();
        if (this.server) {
            return this.server.stop();
        }
    }
    
    async insertTestData() {
        await MealModel.insertMany(mealsTestData);
        await WeekModel.insertMany(weekTestData);
    }

    async cleanup() {
        await MealModel.remove({});
        await WeekModel.remove({});
    }
}

/**
 * Awaits for promise results to return a list of Mongoose interface and returns a new list of all interfaces
 * converted to objects.
 * @param promiseFn Promise function to resolve
 * @returns arr Array of streamlined objects
 */
export const convertInterfaceToObjectList = async <T>(
    promiseFn: Promise<T[]>
): Promise<Array<object>> => {
    let arr: Array<object> = [];

    (await promiseFn).forEach((element: any) => {
        arr.push(element.toObject());
    });

    return arr;
};
