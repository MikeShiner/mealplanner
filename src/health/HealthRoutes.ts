import { Router, Response, Request } from "express";
import * as bodyParser from 'body-parser';
import Mongoose from 'mongoose';


let router = Router();
router.use(bodyParser.json());

router.get('/', (req: Request, res: Response) => {
        res.status(200).end();
});

router.get('/db', (req: Request, res: Response) => {
    if (Mongoose.connection.readyState === 3 ||
        Mongoose.connection.readyState === 0) {
        res.status(500).end();
    } else {
        res.status(200).end();
    }
});

export default router;