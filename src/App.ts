import Server from "./Server";
import { Config } from "./config/Config";

new Server(Config.port).run();