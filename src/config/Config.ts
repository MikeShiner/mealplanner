import rc from 'rc';

export const Config = rc('api', {
    port: 8080,
    mongoHost: 'localhost'
});