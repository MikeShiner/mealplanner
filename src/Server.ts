import http from 'http';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import Mongoose from 'mongoose';
import { Config } from "./config/Config";

import HealthRoutes from '@health/HealthRoutes'
import MealRoutes from '@meals/MealRoutes';
import WeekRoutes from '@weeks/WeekRoutes';
export default class Server {

    private expressApp: express.Application = express();
    private activeServer: http.Server | null = null;

    constructor (private port: number) {
        this.expressApp.use(cors());        
        this.expressApp.use(bodyParser.json());
        this.registerRoutes();
    }
    
    private registerRoutes() {
        this.expressApp.use('/health', HealthRoutes);
        this.expressApp.use('/meals', MealRoutes)
        this.expressApp.use('/week', WeekRoutes)
    }
    
    private async startDatabase() {
        (<any>Mongoose).Promise = global.Promise;
        await Mongoose.connect(`mongodb://${Config.mongoHost}/mealplanner`, { useNewUrlParser: true });        
        console.log(`Mongo connected.`);
    }
    
    async run() {
        this.activeServer = await this.expressApp.listen(this.port);
        this.startDatabase();

        console.log(`API Server started on port ${this.port}`);
    }

    async stop() {
        if(this.activeServer) {
            await Promise.all(
                [this.activeServer.close(), Mongoose.connection.close()]);
                console.log('Http server closed');
        }
    }

    getExpressApp(): express.Application {
        return this.expressApp;
    }
}
