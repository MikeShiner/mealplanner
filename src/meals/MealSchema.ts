import { Schema, Document, model, Model } from "mongoose";

export const MealSchema: Schema = new Schema(
  {
    name: String,
    tags: [String]
  },
  {
    collection: "meals",
    versionKey: false
  }
);

export interface IMeal {
  name: string;
  tags: Array<string>;
}

export interface MealDocument extends IMeal, Document {}

let MealModel: Model<MealDocument>;

try {
  MealModel = model<MealDocument>("Meal", MealSchema);
} catch (e) {
  MealModel = model("Meal");
}

export { MealModel };
