import { IMeal } from "./MealSchema";

export class Meal implements IMeal {
    name: string;
    tags: Array<string>;

    constructor (name:string, tags: Array<string>) {
        this.name = name;
        this.tags = tags;
    }

    toString(): string{
        return '[Meal: name=' + this.name + ', tags=' + this.tags + ']';
    }

    isValid(): boolean {
        return this.name !== undefined && this.tags !== undefined;
    }
}