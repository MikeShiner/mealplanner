import { Router, Request, Response } from 'express';
import bodyParser = require('body-parser');
import { getMeals, addMeal, removeMealsByIdList } from './MealDao';
import { Meal } from './Meal';

let router = Router();
router.use(bodyParser.json());

/**
 * Get all meals matching query string and filter tags
 * @endpoint GET /meals
 * @query params '/meals?search=""&tags=[]'
 */
router.get('/', async (req: Request, res: Response) => {
    try {
        let tags: Array<string> | undefined = undefined;
        if (req.query.tags) tags = JSON.parse(req.query.tags);
        res.json(await getMeals(req.query.search, tags));
    } catch (e) {
        res.status(400).send(e);
    }
});

/**
 * Add a meal
 * @endpoint PUT /meals
 * @body Meal{name:, tags:[]}
 */
router.put('/', async (req: Request, res: Response) => {
    try {
        let newMeal = new Meal(req.body.name, req.body.tags);
        if (!newMeal.isValid()){
            throw new Error(`Meal is not valid. ${newMeal.toString()}`);
        }
        res.json(await addMeal(newMeal));
    } catch (e) {
        console.error(`Error occured! ${e}`);
        res.status(400).send(e);
    }
});

/**
 * Delete a meal
 * @endpoint DELETE /meals
 * @query params '/meals?ids=[]'
 */
router.delete('/', async (req: Request, res: Response) => {
    try {
        res.json(await removeMealsByIdList(JSON.parse(req.query.ids)));
    } catch (e) {
        console.error(`Error occured! ${e}`);
        res.status(400).send(e);
    }
});

export default router;