import { MealModel, MealDocument } from "./MealSchema";
import { Meal } from "./Meal";

/**
 * Get all meals from database by search name and filtered by tags.
 * @param search Partial/full name search
 * @param tags Array of tags to filter by. Filtered by 'all' not 'any'.
 */
export const getMeals = async (
  search?: string,
  tags?: Array<string>): Promise<Array<MealDocument>> => {

  let query: any = {};
  if (search) query.name = new RegExp(search, "i");
  if (tags) query.tags = { $all: tags };

  return await MealModel.find(query);
};

/**
 * Adds new meal to Meal collection
 * @param meal Meal to add
 */
export const addMeal = async (meal: Meal): Promise<MealDocument> => {
  if (!meal.isValid()) throw new Error("Invalid meal" + meal.toString());
  console.log(meal)
  return await new MealModel(meal).save();
};

/**
 * Remove all meals from database by Id
 * @param mealIds Array of Meal Ids to be removed
 */
export const removeMealsByIdList = async (mealIds: Array<string>): Promise<void> => {
  return await mealIds.forEach(
    async id => await MealModel.findByIdAndDelete(id)
  );
};
