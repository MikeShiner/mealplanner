import { Router, Request, Response } from 'express';
import bodyParser = require('body-parser');
import { getWeek, upsertMealToDay, removeMealFromWeek } from './WeekDao';
import { Meal } from '@app/meals/Meal';
let router = Router();
router.use(bodyParser.json());

/**
 * Get all meals for week from <date> to <date> + range(number)
 * @endpoint GET /weeks/<date>
 * @query params '/weeks/2018-01-01?range=
 */
router.get('/:date', async (req: Request, res: Response) => {
    try {
        res.json(await getWeek(req.params.date, req.query.range));
    } catch (e) {
        res.status(500).send(e);
    }
});

/**
 * Add a meal to day
 * @endpoint PUT /weeks/<date>/<lunch|dinner>
 * @body Meal{name:, tags:[]}
 */
router.put('/:date/:type', async (req: Request, res: Response) => {
    let meal: Meal = new Meal(req.body.name, req.body.tags);
    let type: string = req.params.type;

    try {
        if (!meal.isValid()) {
            throw new Error(`Meal is not valid. ${meal.toString()}`);
        }
        if (type !== 'lunch' && type !== 'dinner') {
            throw new Error(`Invalid meal type ${req.params.type}`);
        }
    } catch (e) {
        console.error(`An error occured: ${e}`);
        res.status(400).send(e);
    }

        res.json(await upsertMealToDay(req.params.date, req.params.type, meal));
});

/**
 * Remove a meal from a day. Upsert is the recommend method to replace a meal
 * @endpoint PUT /weeks/<date>/<lunch|dinner>
 * @body Meal{name:, tags:[]}
 */
router.delete('/:date/:type', async (req: Request, res: Response) => {
    try {
        let type: string = req.params.type;
        if (type !== 'lunch' && type !== 'dinner') {
            throw new Error(`Invalid meal type ${req.params.type}`);
        }

        res.json(await removeMealFromWeek(req.params.date, type));
    } catch (e) {
        console.error(`An error occured: ${e}`);
        res.status(500).send(e);
    }
});

export default router;
