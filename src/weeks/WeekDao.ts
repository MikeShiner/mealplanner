import moment from "moment";
import { WeekModel, IWeek } from "./WeekSchema";
import { Meal } from "@meals/Meal";

/**
 * Get all meals within a date range. Range is determined by a start date,
 * and number of days to include.
 * @param startDate Date of the first start date (in ISODate format)
 * @param daysNum Number of days to determine range. Default 7 days.
 */
export const getWeek = async (
  startDate: string,
  daysNum?: number): Promise<Array<IWeek>> => {

  let dateRangeArray: Array<string> = [];
  let startDateMoment: moment.Moment = moment(startDate);
  daysNum = daysNum == undefined ? 7 : daysNum;

  // Generate date range
  for (let _i = 0; _i < daysNum; _i++) {
    let ds = startDateMoment.toISOString().split("T")[0];
    dateRangeArray.push(ds);
    startDateMoment.add(1, "d");
  }

  return await WeekModel.find({ day: { $in: dateRangeArray } }).select("-_id");
};

/**
 * Add a meal to a day
 * @param date Date of the day to add meal to (in ISODate format)
 * @param dayType 'lunch' or 'dinner'
 * @param meal Meal to add to day by dayType
 */
export const upsertMealToDay = async (date: string, dayType: string, meal: Meal): Promise<IWeek| null> => {

  return WeekModel.findOneAndUpdate({day: date}, {[dayType]: meal}, {upsert: true}).exec();
};

/**
 * Unset a meal on a day
 * @param date Date of the day to unset the meal of
 * @param dayType 'lunch' or 'dinner'
 */
export const removeMealFromWeek = async (date: string, dayType: string) => {
    return WeekModel.findOneAndUpdate({day: date}, {$unset:{[dayType]: 1}}, {});    
}