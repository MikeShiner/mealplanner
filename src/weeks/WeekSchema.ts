import { Schema, Document, model, Model } from 'mongoose';
import { MealSchema, IMeal } from '@meals/MealSchema';

export const WeekSchema: Schema = new Schema({
    day: String,
    lunch: MealSchema,
    dinner: MealSchema
}, { collection: 'week' });

export interface Week {
    day: string,
    lunch: IMeal,
    dinner: IMeal
}

export interface IWeek extends Week, Document { };

let WeekModel: Model<IWeek>;

// For integration tests: If model has already been created, return it.
try {
    WeekModel = model<IWeek>("Week", WeekSchema);
} catch (e) {
    WeekModel = model("Week");
}

export { WeekModel }