# Builder image to compile from source
FROM node:10-alpine as builder
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . .
RUN npm install
RUN npx gulp build

# Production image 
FROM node:10-alpine as production
WORKDIR /root
COPY --from=0 /usr/src/app/build/ .
COPY package.json .
RUN npm install --only=prod

EXPOSE 8080
CMD [ "node", "App" ]