var gulp = require('gulp');
var ts = require('gulp-typescript');
var Sequence = require('gulp-sequence')
var sourcemaps = require('gulp-sourcemaps');
var nodemon = require('gulp-nodemon');
var uglify = require('gulp-uglify');
const alias = require('gulp-ts-alias');
var gulpCopy = require('gulp-copy');

const tsProject = ts.createProject('tsconfig.json');

gulp.task('build-compile-ts', function () {
    return gulp.src('src/**/*.ts')
        .pipe(alias({ configuration: tsProject.config }))
        .pipe(tsProject())
        .pipe(gulp.dest('build'));
});

gulp.task('uglify', function () {
    gulp.src(['build/**/*.js'])
        .pipe(uglify())
        .pipe(gulp.dest('build'))
});

gulp.task('copyTestData', function () {
    return gulp.src(['./src/tests/testData/*.*'])
        .pipe(gulpCopy('./build/tests/testData/', {
            prefix: 3
        }));
});


gulp.task('build', Sequence('build-compile-ts', ['uglify', 'copyTestData']));

gulp.task('dev-compile-ts', function () {
    gulp.src('src/**/*.ts')
        .pipe(alias({ configuration: tsProject.config }))
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .pipe(sourcemaps.write('.', { sourceRoot: '../src' }))
        .pipe(gulp.dest('dist'));
});

gulp.task('dev', function (done) {
    var stream = nodemon({
        script: './dist/App',
        ext: 'ts',
        ignore: [
            'node_modules/*',
            'dist/*',
            'build/*'
        ],
        tasks: ['dev-compile-ts', 'copyTestData'],
        done: done
    });
    stream.on('restart', function () {
        console.log('Change detected. Restarting service..');
    });
});